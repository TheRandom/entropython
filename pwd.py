#!/usr/bin/env python3

import random
import string
import math
import getpass

# ANSSI recommandation
LENGTH_ANSSI, ENTROPY_ANSSI = 16, 80

#FIXME: split punctuation with common signs (.!?:;)
dico = string.ascii_letters + string.digits + string.punctuation
print(f"dico = {dico}")

def shannon_entropy(password):
    if not password:
        return 0
    entropy = 0
    alphabet = []
    l = len(password)
    for c in password:
        # if letter add it to alphabet
        if c in string.ascii_letters:
            alphabet.append(string.ascii_letters)
        # if numbers add it to alphabet
        elif c in string.digits:
            alphabet.append(string.digits)
        # if punctuation add it to alphabet
        elif c in string.punctuation:
            alphabet.append(string.punctuation)
        # if unknown char add it to alphabet
        else:
            print("info : this char is not known in our alphabet consider adding it")
            print(f"char = {c}")
            alphabet.append(c)

    alphabet = len(set(alphabet))
    # H = L * log2(N)
    entropy = l * math.log(alphabet, 2)
    return entropy, entropy>=ENTROPY_ANSSI

if __name__ == "__main__":
    # check randomness accuracy if seed is used
    print(f"random number: random.randint(0, 100000000)")

    password = "".join(random.choices(dico, k=LENGTH_ANSSI))
    print(f"password = {password}")

    entropy, isStrong = shannon_entropy(password)
    print(f"entropy = {entropy}")
    print(f"accoring to ANSSI: password is strong = {isStrong}")

    print("Enter your password>", end="")
    password = getpass.getpass()

    entropy, isStrong = shannon_entropy(password)
    print(f"\npassword entropy = {entropy}")
    print(f"accoring to ANSSI: password is strong = {isStrong}")
