# Génération de mot de passe Box (POC)

L'ANSSI dans son guide sur l'authentification expose qu'avec un mot de passe d'au moins 16 caractères, l'authentification est considérée comme étant de forte à très forte.

## But du projet

Ce projet a pour but de créer une fonction python calculant l'entropy afin de donner cette information lors d'une création de compte sur une page web.


# Source

- [taille mdp ANSSI](https://www.ssi.gouv.fr/administration/precautions-elementaires/calculer-la-force-dun-mot-de-passe/)
